package foo;

public class SituationImpl implements Situation {
    private int _damage;
    private int _distance;

    public SituationImpl(int _damage, int _distance) {
        this._damage = _damage;
        this._distance = _distance;
    }

    public int distance() {
        return _distance;
    }

    public int damage() {
        return _damage;
    }
}
