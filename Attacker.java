package foo;

public interface Attacker {
    public int range();
    public int level();
    public String faction();
}
