package foo;

import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CharacterTest {

    private Character makeNewCharacter(){
        return new Character(1, "Ironman");
    }

    private Character missileAttackerLevel1=new Character(1); //TODOmake this a missile attacker
    private Character missileAttackerLevel6=new Character(6);

    private Situation oneHundredHitpointsCloseRange = new SituationImpl(100, 1);
    private Situation oneHundredHitpointsVeryLongRange = new SituationImpl(100, 100);
    private Situation oneThousandHitpointsCloseRange = new SituationImpl(1000, 1);
    private Situation tenThousandHitpointsCloseRange = new SituationImpl(10000, 1);

    @Test
    public void initialHealthTest() {
        Character monster1 = makeNewCharacter();
        assertEquals(monster1.health(), 1000);
    }

    @Test
    public void initiallyAlive() {
        Character monster1 = makeNewCharacter();
        assertTrue(monster1.is_alive());
    }

    @Test
    public void damageTest() {
        Character monster1 =  makeNewCharacter();
        monster1.damage(oneHundredHitpointsCloseRange, missileAttackerLevel1);
        assertEquals(monster1.health(), 900);
    }

    @Test
    public void theCharacterIsAliveWhenLittleDamage() {
        Character monster1 =  makeNewCharacter();
        monster1.damage(oneHundredHitpointsCloseRange, missileAttackerLevel1);
        assertEquals(true, monster1.is_alive());
    }

    @Test
    public void theCharacterDiesWhen1000Damage() {
        Character monster1 = makeNewCharacter();
        monster1.damage(oneThousandHitpointsCloseRange, missileAttackerLevel1);
        assertEquals(false, monster1.is_alive());
    }

    @Test
    public void theCharacterDiesWhenTooMuchDamage() {
        Character monster1 =  makeNewCharacter();
        monster1.damage(tenThousandHitpointsCloseRange, missileAttackerLevel1);
        assertEquals(false, monster1.is_alive());
    }

    @Test
    public void theCharacterHasZeroHealthWhenTooMuchDamage() {
        Character monster1 =  makeNewCharacter();
        monster1.damage(tenThousandHitpointsCloseRange, missileAttackerLevel1);
        assertEquals(0, monster1.health());
    }

    @Test
    public void damageWhenTheAttackerIsFiveOrMoreLevelAboveTargetWithinRange() {
        Character monster = makeNewCharacter();
        monster.damage(oneHundredHitpointsCloseRange, missileAttackerLevel6);
        assertEquals(850, monster.health());
    }

    @Test
    public void damageWhenTheAttackerIsFiveOrMoreLevelAboveTargetBeyondRange() {
        Character monster = makeNewCharacter();
        monster.damage(oneHundredHitpointsVeryLongRange, missileAttackerLevel6);
        assertEquals(1000, monster.health());
    }

    @Test
    public void damageWhenTheAttackerIsFiveOrMoreLevelBelowTargetWithinRange() {
        Character monster = new Character(6);
        monster.damage(oneHundredHitpointsCloseRange, missileAttackerLevel1);
        assertEquals(950, monster.health());
    }

    @Test
    public void damageWhenTheAttackerIsFiveOrMoreLevelBelowTargetBeyondRange() {
        Character monster = new Character(6);
        monster.damage(oneHundredHitpointsVeryLongRange, missileAttackerLevel1);
        assertEquals(1000, monster.health());
    }

    @Test
    public void sameFactionCannotMakeDamage() {
        Character monster =  makeNewCharacter();
        monster.damage(oneHundredHitpointsCloseRange, missileAttackerLevel6);
        assertEquals(1000, monster.health());
    }



}
