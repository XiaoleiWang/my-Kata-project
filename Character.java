package foo;



public class Character implements Attacker{
    private int _health = 1000;
    private boolean _alive = true;
    private int _level = 1;
    private int _range = Range.MeleeRange;
    private String _faction = "IronMan";



    public  Character(int level, String faction){
        this._level = level;
        this._faction = faction;
    }

    public int health() {
        return _health;
    }

    public boolean is_alive() {
        return _alive;
    }


    private int calcDamage(int rawDamage, int levelDiff){
        if (levelDiff >= 5) {
            return (int) (rawDamage*1.5);
        }
        else {
            return (int) (rawDamage*0.5);
        }

    }
    public void damage(Situation situation, Attacker attacker ) {
        if (situation.distance() <= attacker.range()) {
            int damageValue = calcDamage(situation.damage(), attacker.level() - _level);
            _health = Math.max(0, _health - damageValue);
            _alive = _health > 0;
        }
    }

    public String faction() {
        return _faction;
    }

    public int range() {
        return _range;
    }

    public int level() {
        return _level;
    }
}
