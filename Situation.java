package foo;

public interface Situation {
    public int distance();
    public int damage();
}
